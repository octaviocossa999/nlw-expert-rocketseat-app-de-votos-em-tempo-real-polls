import { PrismaClient } from "@prisma/client";
import { array } from "zod";

export const prisma = new PrismaClient({
    log:['query']
});
